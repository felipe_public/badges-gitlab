"""Unit tests for badges_api module."""
import os
import shutil
import unittest
from io import StringIO
from unittest.mock import patch

from badges_gitlab import badges_api

fixture_directory_not_exists = os.path.join(os.getcwd(), "tests", "test_not_exist")
fixture_json = os.path.join(os.getcwd(), "tests", "json")
fixture_svg_location = os.path.join(os.getcwd(), "tests", "svg")


class TestAPIBadges(unittest.TestCase):
    """Tests for the badges api module."""

    def test_validate_path(self):
        """Test for validate path."""
        expected_value = f"Directory  {fixture_directory_not_exists}  created!\n"
        with patch("sys.stdout", new=StringIO()) as fake_out:
            badges_api.validate_path(fixture_directory_not_exists)
            self.assertEqual(fake_out.getvalue(), expected_value)

    def tearDown(self) -> None:
        """Teardown, deletes any created files."""
        shutil.rmtree(fixture_directory_not_exists)


if __name__ == "__main__":
    unittest.main()
