"""Unit tests for badges_json module."""
import os
import shutil
import unittest
from io import StringIO
from unittest.mock import patch

from badges_gitlab import badges_json

fixture_directory_not_exists = os.path.join(os.getcwd(), "tests", "test_not_exist")
fixture_json = os.path.join(os.getcwd(), "tests", "json")
fixture_svg_location = os.path.join(os.getcwd(), "tests", "svg")


class TestBadgesJSON(unittest.TestCase):
    """Test for the badges json module."""

    def test_print_json(self):
        """Test for printing badges in json."""
        expects = {
            "schemaVersion": 1,
            "label": "some",
            "message": "msg",
            "color": "different-color",
        }
        self.assertEqual(
            badges_json.print_json("some", "msg", "different-color"), expects
        )

    def test_json_badge(self):
        """Test for generating the badge in json format."""
        expects = {
            "schemaVersion": 1,
            "label": "some",
            "message": "msg",
            "color": "different-color",
        }
        filename = "test"
        os.makedirs(fixture_json, exist_ok=True)
        with patch("sys.stdout", new=StringIO()):
            badges_json.json_badge(fixture_json, filename, expects)
            path_to_assert = os.path.join(fixture_json, f"{filename}.json")
            self.assertTrue(
                os.path.isfile(path_to_assert),
                f"Path tested was {path_to_assert}",
            )
        shutil.rmtree(fixture_json)


if __name__ == "__main__":
    unittest.main()
