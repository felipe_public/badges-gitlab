"""Unit tests for badges_svg module."""
import os
import shutil
import unittest

from badges_gitlab import badges_svg

fixture_directory_not_exists = os.path.join(os.getcwd(), "tests", "test_not_exist")
fixture_json = os.path.join(os.getcwd(), "tests", "json")
fixture_svg_location = os.path.join(os.getcwd(), "tests", "svg")


class TestBadgesSVG(unittest.TestCase):
    """Tests for the badges json module."""

    def test_replace_space(self):
        """Test for function replacing spaces in string."""
        string_with_spaces = "some string with spaces"
        expected_string = "some_string_with_spaces"
        self.assertEqual(badges_svg.replace_space(string_with_spaces), expected_string)

    def test_validate_json_path(self):
        """Test for validating path for json file."""
        os.makedirs(fixture_svg_location, exist_ok=True)
        with open(
            os.path.join(fixture_svg_location, "some.json"), "w", encoding="utf-8"
        ):
            self.assertTrue(badges_svg.validate_json_path(fixture_svg_location))
        shutil.rmtree(fixture_svg_location)


if __name__ == "__main__":
    unittest.main()
