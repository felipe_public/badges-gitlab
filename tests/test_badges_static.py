"""Unit tests for badges_static module."""
import os
import shutil
import unittest
from io import StringIO
from unittest.mock import patch

import vcr  # type: ignore

from badges_gitlab import badges_static

fixture_directory_not_exists = os.path.join(os.getcwd(), "tests", "test_not_exist")
fixture_json = os.path.join(os.getcwd(), "tests", "json")
fixture_svg_location = os.path.join(os.getcwd(), "tests", "svg")


class TestBadgesStatic(unittest.TestCase):
    """Tests for badges static module."""

    def test_convert_to_snake_case(self):
        """Test conversion to snake case."""
        test_word = "label test"
        expected_result = "label_test"
        result = badges_static.to_snake_case(test_word)
        self.assertEqual(result, expected_result)

    def test_convert_list_json_badge(self):
        """Test if it can convert the lists into json badges."""
        test_list_ok = unittest.TestCase.subTest
        with test_list_ok(self):
            badges_list = [
                ["label test", "msgtest", "green"],
                ["label 2test", "msgtest", "blue"],
            ]
            expected_result = [
                {
                    "schemaVersion": 1,
                    "label": "label test",
                    "message": "msgtest",
                    "color": "green",
                },
                {
                    "schemaVersion": 1,
                    "label": "label 2test",
                    "message": "msgtest",
                    "color": "blue",
                },
            ]
            result = badges_static.convert_list_json_badge(badges_list)
            self.assertEqual(result, expected_result)

        test_list_wrong = unittest.TestCase.subTest
        with test_list_wrong(self):
            json_list = ["some", "weird", "list", "not compatible"]
            expected_result = []
            result = badges_static.convert_list_json_badge(json_list)
            self.assertEqual(result, expected_result)

    def test_print_static_badges(self):
        """Test if it can print/create static badges."""
        fixtures_dir = "tests/fixtures/static/"
        os.makedirs(fixtures_dir, exist_ok=True)
        badge_result = ["label test", "msg test", "green"]
        badges_list = [badge_result]
        with patch("sys.stdout", new=StringIO()) as fake_out:
            badges_static.print_static_badges(fixtures_dir, badges_list)
            self.assertEqual(
                fake_out.getvalue(),
                "Creating JSON Badge file for label test ... Done!\n",
            )
        shutil.rmtree(fixtures_dir)

    def test_extract_svg_title(self):
        """Test if can extract from svg the badge label."""
        test_file_ok = unittest.TestCase.subTest
        with test_file_ok(self):
            fixture_svg = os.path.join("tests", "fixtures", "badge.svg")
            expected_result = "conventional_commits"
            with open(fixture_svg, encoding="utf-8") as svg_file:
                result = badges_static.extract_svg_title(svg_file.read())
                self.assertEqual(result, expected_result)

        test_file_wrong = unittest.TestCase.subTest
        with test_file_wrong(self):
            fixture_svg = os.path.join("tests", "fixtures", "report.xml")
            expected_result = ""
            with open(fixture_svg, encoding="utf-8") as svg_file:
                result = badges_static.extract_svg_title(svg_file.read())
                self.assertEqual(result, expected_result)

    @vcr.use_cassette()
    def test_download_badges(self):
        """Test if it can correclty dowload badges using a playback."""
        test_fixtures_dir = os.path.join("tests", "fixtures")

        test_link_ok = unittest.TestCase.subTest
        with test_link_ok(self):
            url = ["https://img.shields.io/badge/conventional%20commits-1.0.0-yellow"]
            with patch("sys.stdout", new=StringIO()):
                badges_static.download_badges(test_fixtures_dir, url)
                result = os.path.join(test_fixtures_dir, "conventional_commits.svg")
                self.assertTrue(os.path.isfile(result))
                if os.path.exists(result):
                    os.remove(result)

        test_link_wrong = unittest.TestCase.subTest
        with test_link_wrong(self):
            url = ["https://gitlab.com/felipe_public/badges-gitlab"]
            with patch("sys.stdout", new=StringIO()) as fake_out:
                badges_static.download_badges(test_fixtures_dir, url)
                self.assertEqual(
                    fake_out.getvalue(),
                    "Incompatible link from shields.io links, skipping...\n",
                )


if __name__ == "__main__":
    unittest.main()
