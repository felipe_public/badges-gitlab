"""Unit tests for badges_test module."""
import os
import re
import unittest
from io import StringIO
from unittest.mock import patch

from junitparser import TestSuite  # type: ignore

from badges_gitlab import badges_test

fixture_directory_not_exists = os.path.join(os.getcwd(), "tests", "test_not_exist")
fixture_json = os.path.join(os.getcwd(), "tests", "json")
fixture_svg_location = os.path.join(os.getcwd(), "tests", "svg")


class TestBadgesTest(unittest.TestCase):
    """Test for badges test module."""

    def setUp(self) -> None:
        """Setup for the tests."""
        self.json_test_directory = "tests/fixtures"

    def test_create_badges_test(self):
        """Test if badges are created from test report."""
        test_file_does_not_exist = unittest.TestCase.subTest
        with test_file_does_not_exist(self):
            xml_path = "tests/report_not_exist.xml"
            self.assertEqual(
                badges_test.create_badges_test(self.json_test_directory, xml_path),
                "Junit report file does not exist...skipping!",
            )

        test_wrong_file_type = unittest.TestCase.subTest
        with test_wrong_file_type(self):
            file_path = "pyproject.toml"
            self.assertEqual(
                badges_test.create_badges_test(self.json_test_directory, file_path),
                "Error parsing the file. Is it a JUnit XML?",
            )

        test_create_badges = unittest.TestCase.subTest
        with patch("sys.stdout", new=StringIO()):
            with test_create_badges(self):
                xml_path = "tests/fixtures/report.xml"
                regex = re.search(
                    r"Badges from JUnit XML test report tests created!",
                    badges_test.create_badges_test(self.json_test_directory, xml_path),
                )
                self.assertTrue(regex)

        test_single_testsuite = unittest.TestCase.subTest
        with patch("sys.stdout", new=StringIO()):
            with test_single_testsuite(self):
                xml_path = "tests/fixtures/singletestsuite.xml"
                regex = re.search(
                    r"Badges from JUnit XML test report tests created!",
                    badges_test.create_badges_test(self.json_test_directory, xml_path),
                )
                self.assertTrue(regex)

        test_no_testsuites = unittest.TestCase.subTest
        with patch("sys.stdout", new=StringIO()):
            with test_no_testsuites(self):
                xml_path = "tests/fixtures/notestsuites.xml"
                regex = re.search(
                    r"Badges from JUnit XML test report tests created!",
                    badges_test.create_badges_test(self.json_test_directory, xml_path),
                )
                self.assertTrue(regex)

    def tests_statistics(self):
        """Test if it can parses the project statistics correctly."""
        stats_tests_dict = {
            "total_tests": 0,
            "total_failures": 0,
            "total_errors": 0,
            "total_skipped": 0,
            "total_time": 0.4,
        }
        testsuite = TestSuite("suite1")
        testsuite.tests = 3
        testsuite.failures = 2
        testsuite.errors = 0
        testsuite.skipped = 0
        testsuite.time = 0.4
        expected_result_dict = {
            "total_tests": 3,
            "total_failures": 2,
            "total_errors": 0,
            "total_skipped": 0,
            "total_time": 0.8,
        }
        test_result = badges_test.tests_statistics(stats_tests_dict, testsuite)
        self.assertEqual(expected_result_dict, test_result)

    def test_create_json_test_badges(self):
        """Test if it creates json badges from test  report data."""
        with patch("sys.stdout", new=StringIO()):
            fixture_list = [11, 2, 0, 0, 0.014]
            total_passed = fixture_list[0] - sum(fixture_list[1:4])
            regex = re.search(
                r"Total Tests = {}, Passed = {}, Failed = {}, "
                r"Errors = {}, Skipped = {}, Time = {:.2f}s.".format(
                    fixture_list[0],
                    total_passed,
                    fixture_list[1],
                    fixture_list[2],
                    fixture_list[3],
                    fixture_list[4],
                ),
                badges_test.create_test_json_badges(
                    self.json_test_directory, fixture_list
                ),
            )
            self.assertTrue(regex)

    def tearDown(self):
        """Deletes files from test directory."""
        files = os.listdir(self.json_test_directory)
        for file in files:
            if file.endswith(".json"):
                os.remove(os.path.join(self.json_test_directory, file))


if __name__ == "__main__":
    unittest.main()
