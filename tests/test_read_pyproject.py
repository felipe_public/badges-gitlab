"""Unit tests for badges_gitlab package."""
import os
import unittest
from io import StringIO
from unittest.mock import patch

from badges_gitlab import read_pyproject

fixture_directory_not_exists = os.path.join(os.getcwd(), "tests", "test_not_exist")
fixture_json = os.path.join(os.getcwd(), "tests", "json")
fixture_svg_location = os.path.join(os.getcwd(), "tests", "svg")


class TestReadPyProject(unittest.TestCase):
    """Test for read pyproject module."""

    def setUp(self) -> None:
        """Setup of the test."""
        self.fixture_pyproject = os.path.join("tests", "fixtures")

    def test_pyproject_exists(self):
        """Test if function will return if file exists."""
        target = os.path.join(self.fixture_pyproject, "pyproject.toml")
        self.assertTrue(read_pyproject.pyproject_exists(target))

    def test_load_pyproject(self):
        """Test if it can loads the pyproject file."""
        test_wrong_file_type = unittest.TestCase.subTest
        with test_wrong_file_type(self):
            target = os.path.join(self.fixture_pyproject, "pyproject_wrong.toml")
            with patch("sys.stdout", new=StringIO()) as fake_out:
                read_pyproject.load_pyproject(target)
                self.assertEqual(fake_out.getvalue(), "Incompatible .toml file!\n")

        test_section_not_found = unittest.TestCase.subTest
        with test_section_not_found(self):
            target = os.path.join(self.fixture_pyproject, "pyproject_no_section.toml")
            with patch("sys.stdout", new=StringIO()) as fake_out:
                read_pyproject.load_pyproject(target)
                self.assertEqual(
                    fake_out.getvalue(),
                    'The "badges_gitlab" section in pyproject.toml was not found!\n',
                )


if __name__ == "__main__":
    unittest.main()
