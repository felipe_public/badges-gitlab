"""Unit tests for cli module."""
import os
import sys
import unittest
from io import StringIO
from unittest.mock import patch

from badges_gitlab import __version__ as version
from badges_gitlab import cli

fixture_directory_not_exists = os.path.join(os.getcwd(), "tests", "test_not_exist")
fixture_json = os.path.join(os.getcwd(), "tests", "json")
fixture_svg_location = os.path.join(os.getcwd(), "tests", "svg")


class TestCLI(unittest.TestCase):
    """Tests for the cli module."""

    def test_cli_parse_args_version(self):
        """Test if argument version is parsed."""
        parser = cli.parse_args(["-V"])
        self.assertTrue(parser.version)

    def test_cli_main_version(self):
        """Test if cli outputs the version correcly."""
        with patch("sys.stdout", new=StringIO()) as fake_out:
            try:
                sys.argv = ["prog", "-V"]
                cli.main()
            except SystemExit:
                pass
            self.assertEqual(fake_out.getvalue(), f"badges-gitlab v{version}\n")

    def test_cli_parse_args_badges(self):
        """Test if arguments for custom badges are parsed."""
        parser = cli.parse_args(["-s", "conventional commits", "1.0.1", "yellow"])
        expected_result = [["conventional commits", "1.0.1", "yellow"]]
        self.assertEqual(parser.static_badges, expected_result)

    def test_cli_parse_args_link_badges(self):
        """Test if arguments using badges links are parsed."""
        parser = cli.parse_args(
            [
                "-lb",
                "https://img.shields.io/pypi/v/badges-gitlab",
                "https://img.shields.io/pypi/wheel/badges-gitlab",
            ]
        )
        expected_result = [
            "https://img.shields.io/pypi/v/badges-gitlab",
            "https://img.shields.io/pypi/wheel/badges-gitlab",
        ]
        self.assertEqual(parser.link_badges, expected_result)


if __name__ == "__main__":
    unittest.main()
