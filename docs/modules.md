## Modules Documentation

### CLI
```{eval-rst}
.. automodule:: badges_gitlab.cli
   :members: 
```
### Badges API
```{eval-rst}
.. automodule:: badges_gitlab.badges_api
   :members: 
```
### Badges JSON
```{eval-rst}
.. automodule:: badges_gitlab.badges_json
   :members: 
```
### Badges Static
```{eval-rst}
.. automodule:: badges_gitlab.badges_static
   :members: 
```
### Badges SVG
```{eval-rst}
.. automodule:: badges_gitlab.badges_svg
   :members: 
```
### Badges Test
```{eval-rst}
.. automodule:: badges_gitlab.badges_test
   :members: 
```
### Read pyproject.toml
```{eval-rst}
.. automodule:: badges_gitlab.read_pyproject
   :members: 
```

